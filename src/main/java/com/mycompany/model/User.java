package com.mycompany.model;

import javax.validation.constraints.*;

public class User {
    @NotNull
    @Size(message = "Last name must be between 2 and 20 characters", min = 2, max = 50)
    private String lastName;
    @NotNull
    @Size(message = "The name must be between 2 and 20 characters", min = 2, max = 50)
    private String firstName;
    private String middleName;
    @Min(value = 0, message = "Minimum age 1")
    @Max(value = 150, message = "Maximum age 150")
    private int age;
    private int salary;
    @Email(message = "The mail must match the format: xxx@xxx.xxx")
    private String email;
    private String company;

    public User(String lastName, String firstName, String middleName, int age, int salary, String email, String company) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.age = age;
        this.salary = salary;
        this.email = email;
        this.company = company;
    }

    public User() {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "#" + lastName +
                "#" + firstName +
                "#" + middleName +
                "#" + age +
                "#" + salary +
                "#" + email +
                "#" + company;
    }
}
