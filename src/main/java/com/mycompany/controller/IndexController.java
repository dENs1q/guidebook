package com.mycompany.controller;

import com.mycompany.model.Mail;
import com.mycompany.model.User;
import com.mycompany.service.EmailService;
import com.mycompany.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Controller
public class IndexController {

    @Autowired
    private OperationService operationService;
    @Autowired
    private EmailService emailService;

    @GetMapping("/index")
    public String indexForm(Model model) {
        model.addAttribute("user", new User());
        return "index";
    }

    @PostMapping("/index")
    public String userSubmit(@ModelAttribute @Valid User user, BindingResult bindingResult) throws IOException {
        if (bindingResult.hasErrors()) {
            return "index";
        }
        operationService.writeUser(user);
        return "result";
    }

    @PostMapping("/upload")
    public String userUpload(@RequestParam("newFile") MultipartFile newFile) throws IOException {
        if (!newFile.isEmpty()) {
            operationService.multipartFileToFile(newFile);
            List<User> usersList = operationService.parseUser(new File("bdTemp.txt"));
            for (User user : usersList) {
                operationService.writeUser(user);
            }
            return "upload";
        }
        return "index";
    }

    @GetMapping("/search")
    public String searchUser(Model model, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName) throws IOException {
        User user = operationService.searchUser(lastName, firstName);
        if (user != null) {
            model.addAttribute("user", user);
            model.addAttribute("mail", new Mail());
            return "search";
        } else {
            model.addAttribute("lastName", lastName);
            model.addAttribute("firstName", firstName);
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
        }
    }

    @PostMapping("/search")
    public String userSubmit(@ModelAttribute Mail mail, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "search";
        }
        emailService.sendSimpleMessage(mail);
        return "emailSend";
    }
}
