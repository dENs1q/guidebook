package com.mycompany.service;

import com.mycompany.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface OperationService {

    void writeUser(User user) throws IOException;

    User searchUser(String lastName, String firstName) throws IOException;

    List<User> parseUser(File file) throws FileNotFoundException;

    void multipartFileToFile(MultipartFile mFile) throws IOException;

}
