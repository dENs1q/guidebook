package com.mycompany.service;

import com.mycompany.model.Mail;

public interface EmailService {
    void sendSimpleMessage(Mail mail);
}
