package com.mycompany.service.impl;

import com.mycompany.model.User;
import com.mycompany.service.OperationService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Service
public class OperationServiceImpl implements OperationService {
    private static String bdName = "bd.txt";
    private static String pathTemp = "bdTemp.txt";

    @Override
    public void writeUser(User user) throws IOException {
        FileWriter writer = new FileWriter(bdName, true);
        writer.write(user.toString() + "\r\n");
        writer.close();
    }

    @Override
    public User searchUser(String lastName, String firstName) throws IOException {
        File file = new File(bdName);
        List<User> usersList = parseUser(file);
        for (User user : usersList) {
            if (user.getLastName().equalsIgnoreCase(lastName) &&
                    user.getFirstName().equalsIgnoreCase(firstName)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public List<User> parseUser(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        List<User> usersList = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] iline = line.split("#", 8);
            String userLastName = iline[1];
            String userFirstName = iline[2];
            String userMiddleName = iline[3];
            int userAge = Integer.parseInt(iline[4]);
            int userSalary = Integer.parseInt(iline[5]);
            String userEmail = iline[6];
            String userCompany = iline[7];

            usersList.add(new User(userLastName, userFirstName, userMiddleName, userAge, userSalary, userEmail, userCompany));
        }
        return usersList;
    }

    @Override
    public void multipartFileToFile(MultipartFile mFile) throws IOException {
        FileWriter writer = new FileWriter(pathTemp, false);
        writer.write(new String(mFile.getBytes()));
        writer.close();
    }
}
