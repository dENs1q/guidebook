package com.mycompany.service.impl;

import com.mycompany.model.Mail;
import com.mycompany.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Value("${spring.mail.username}")
    private String fromUser;

    @Override
    public void sendSimpleMessage(Mail mail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(fromUser);
        message.setTo(mail.getTo());
        message.setSubject(mail.getSubject());
        message.setText(mail.getContent());
        emailSender.send(message);

    }

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.yandex.ru");
        mailSender.setPort(465);

        mailSender.setUsername("asdafdgdbtrfer@yandex.ru");
        mailSender.setPassword("1qazxsw23edc");

        Properties properties = new Properties();
        properties.put("mail.smtp.ssl.enable", true);
        properties.put("mail.default-encoding", "UTF-8");
        properties.put("mail.host", "smtp.yandex.ru");
        properties.put("mail.username", "asdafdgdbtrfer@yandex.ru");
        properties.put("mail.password", "1qazxsw23edc");
        properties.put("mail.port", "465");
        properties.put("mail.protocol", "smtp");
        properties.put("mail.test-connection", "false");
        properties.put("mail.properties.mail.smtp.auth", "true");
        properties.put("mail.properties.mail.smtp.starttls.enable", "true");
        mailSender.setJavaMailProperties(properties);
        return mailSender;
    }
}